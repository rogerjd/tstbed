package array

import "fmt"

// assingn one array to another, modify it, and see if the change is
// in the other array too
func valOrRef() {
	a1 := [3]int{1, 2, 3}
	a2 := a1  //after this, a2 == a1 (same values)
	a1[1] = 9 //after this a2 is unchanged
	fmt.Printf("%v", a2)
}
