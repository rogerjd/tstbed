package array

import (
	"fmt"
)

//array is value type

func Main(op string) {
	//ref: must use println to close, print wont output till close
	fmt.Println("Array Main")

	switch op {
	case "make":
		makeArray()
		return
	case "ValOrRef":
		valOrRef()
	case "fallthrough":
		//doesnt fall through, unless there is fallthrough stmt
		fmt.Printf("fallthrough\n")
	case "param":
		param()
	default:
		println("op not found: " + op)
	}
}
