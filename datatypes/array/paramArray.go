package array

import (
	"fmt"
)

type (
	t  [3]int
	t4 [3]int
	t5 t
)

func param() {
	a := [3]int{}
	fmt.Println(a)
	byValue(a)
	fmt.Println(a)

	a3 := t{}
	fmt.Println(a3)
	byValue(a3)


	byAddr(&a)
	fmt.Println(a)
	typeParam(a)

	a2 := [3]int{}
	fmt.Println(a2)
	//	typeParam(a2)  error: type mismatch (size)

	t2 := t{}
	typeParam(t2)

	//	t5 := t4{}
	//	typeParam(t5)
}

func byValue(n [3]int) {
	n[0] = 9
	fmt.Printf("in byValue %v\n", n)
}

func byAddr(n *[3]int) {
	n[0] = 9
	fmt.Printf("in byAddr %v\n", n)
}

func typeParam(a t) {
	fmt.Printf("typeParam: %d\n", len(a))
}
