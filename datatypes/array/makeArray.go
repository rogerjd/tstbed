package array

import (
	"fmt"
)

type (
	a1 = [5]int
)

func makeArray() {
	fmt.Printf("make array \n")

	//arrays have size [5]int{} or implicit size [...](compiler counts elements)
	//  slice has none []int
	n := [5]int{}
	fmt.Print(n)

	m := [5]int{1, 2, 3} //zero fill
	fmt.Print(m)

	var m2 *[6]int
	m2 = new([6]int) //returns pointer to new allocated and zero-filled memory
	fmt.Print(m2)

}
