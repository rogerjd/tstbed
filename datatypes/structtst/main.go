package structtst

import "fmt"

//struct are value types

func Main(op string) {
	fmt.Println("struct")

	switch op {
	case "ValOrRefAssign":
		valOrRefAssign()
	default:
		println("op not found: " + op)
	}
}

type tst struct {
	name string
}

func valOrRefAssign() {
	a := tst{"abc"}
	b := a
	fmt.Println(a, b)
	b.name = "bcd"
	fmt.Println(a, b)
}
