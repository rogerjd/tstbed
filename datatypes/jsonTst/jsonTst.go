package jsonTst

/*
  Encode/Decode to/from go type
  Marshal/Unmarshal to/from []byte
*/

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
)

type (
	Message struct {
		Name string
		Body string
		Time int64
	}
	Msg Message
)

func (m *Message) UnmarshalJSON(buf []byte) (err error) {
	//	var msg Message
	var msg2 Msg

	if err = json.Unmarshal(buf, &msg2); err != nil {
		return err
	}

	//msg = Message{"a", "b", 0}

	*m = Message{
		Name: "Joe",
	}
	return nil
}

//Main is menu for sub pkg
func Main(cmd string) {

	fmt.Println("jsonTst, cmd:", cmd)

	switch cmd {
	case "marshal":
	case "":
		Marshal_test()
	case "decode":
		decodeTest()
	case "encode":
		encodeTest()
	}
}

//decode calls UnmarshalJSON ?

//ref: Marshal and Unmarshal are not inverse all the time Summerfield
func Marshal_test() {
	m := Message{"Alice", "Hello", 1249706}
	b, err := json.Marshal(m)
	if err != nil {
		print(err)
		return
	} else {
		println(string(b))
	}

	//decode
	var m1 Message
	err = json.Unmarshal(b, &m1)
	if err != nil {
		print(err)
	} else {
		fmt.Printf("%+v", m1)
	}
}

func encodeTest() {
	var buf bytes.Buffer
	e := json.NewEncoder(&buf)
	e.Encode(123)
	fmt.Println(buf.String())

	to, err := os.OpenFile("C:/Users/roger/go/src/bitbucket.org/rogerjd/tstbed/datatypes/jsonTst/enocde.json", os.O_RDWR|os.O_CREATE, 0666)
	defer to.Close()
	if err != nil {
		println(err)
		return
	}

	//write num
	e = json.NewEncoder(to)
	e.Encode(123)

	//write str
	e.Encode("abc")

	//write slice
	slc := []string{"apple", "peach", "lettuce"}
	e.Encode(slc)

	//write map
	m1 := map[string]int{"apple": 5, "lettuce": 2}
	e.Encode(m1)

	m := Message{"Alice", "Hello", 1249706}
	e.Encode(m)
}

func decodeTest() {
	println("decodeTest")
	to, err := os.Open("C:/Users/roger/go/src/bitbucket.org/rogerjd/tstbed/datatypes/jsonTst/enocde.json")
	defer to.Close()
	if err != nil {
		println(err)
		return
	}

	e := json.NewDecoder(to)
	var (
		n   int
		s   string
		slc []string
		m2  map[string]int
		m   Message
	)

	e.Decode(&n)
	println(n)

	e.Decode(&s)
	println(s)

	e.Decode(&slc)
	e.Decode(&m2)
	fmt.Printf("%v", slc)
	fmt.Printf("%v", m2)

	for i := range slc {
		println(slc[i])
	}

	e.Decode(&m)
	fmt.Printf("%v", m)
}
