package maptest

import "fmt"

/*
https://blog.golang.org/go-maps-in-action

maps are reference types (like, pointer and slices)

KeyType may be any type that is comparable

Note that since both range and len treat a nil slice as a zero-length slice,
	these last two examples will work even if nobody likes cheese or bacon (however unlikely that may be).

It can be convenient that a map retrieval yields a zero value when the key is not present.
	For instance, a map of boolean values can be used as a set-like data structure (recall that the zero value for the boolean type is false)
	There's no need to use the two-value form to test for the presence of n in the map; the zero value default does it for us.

 1.12: Maps are now printed in key-sorted order to ease testing. The ordering rules are:
    prt different that range(iterate)?
?	iterating over a map with a range loop, the iteration order is not specified
 	If you require a stable iteration order you must maintain a separate data structure
	that specifies that order. This example uses a separate sorted slice of keys to
	print a map[int]string in key order: slice of keys eg: int, then sort sort.Ints(keys)
	range over keys, and use each ones value as map key
*/

func Main(op string) {
	//ref: must use println to close, print wont output till close
	fmt.Println("Map Test, maptest")

	switch op {
	case "make":
		makeMap()
		return
	case "ValOrRef":
		//valOrRef()
	case "fallthrough":
		//doesnt fall through, unless there is fallthrough stmt
		fmt.Printf("fallthrough\n")
	case "param":
		//param()
	default:
		println("op not found: " + op)
	}
}

func makeMap() {
	m := make(map[string]int)
	m2 := map[string]int{} //empty map, same as make

	//Write w
	m["abc"] = 123

	//map literal, to initialize with data
	commits := map[string]int{"rsc": 3711, "r": 2138, "gri": 1908,
		"adg": 912}

	delete(m, "abc") //return nothing (not a prob if key doesnt exist)

	//Read R
	i := m["abc"] //if key doesnt exist, will return the zero value for the value type(int here)

	//test for key
	i, ok := m["abc"] //ok bool, key exists
	_, ok = m["abc"]  //test key exists only

	//iterate
	for key, val := range m {
		fmt.Println("Key: ", key, "Value: ", val)
	}

	//info metadata
	n := len(m) //length of map number of entries elements
}
