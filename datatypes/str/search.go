package str

import "strings"

func SearchTst() {
	t := "test"

	//str, zero based
	println(strings.Index(t, "t"))  //ref: 0
	println(strings.Index(t, "z"))  //ref: -1
	println(strings.Index(t, "es")) //ref: 0

	println(strings.Contains(t, "e"))

}
