package str

// " "
// raw string  ` abc `  back quotes
/*
`line 1
line 2
line 3`
*/

/*
	strconv package
		ParseInt str to int
		FormatInt int to str
*/

func Main(op string) {
	//ref: must use println to close, print wont output till close
	println("string test z")

	switch op {
	case "search":
		SearchTst()
		return
	case "fields":
		FieldsTst()
		return
	case "concat":
		strConcat()
	case "split":
		split()
	case "join":
		join()
	default:
		println("op not found: " + op)
	}
}
