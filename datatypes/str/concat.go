package str

import (
	"bytes"
	"fmt"
	"strings"
)

//string concat append string builder

/*
instead of: (slow)
s := ""
for i := 0; i < 1000; i++ {
    s += getShortStringFromSomewhere()
}
return s

use: (fast) append

use: (fast)
 var buffer bytes.Buffer

    for i := 0; i < 1000; i++ {
        buffer.WriteString("a")
    }

    fmt.Println(buffer.String())
*/

func strConcat() {
	fmt.Println("String concat")

	var buffer bytes.Buffer

	for i := 0; i < 1000; i++ {
		buffer.WriteString("a")
	}
	fmt.Println(buffer.String())

	s := make([]string, 0)
	for i := 0; i < 1000; i++ {
		s = append(s, "z")	
	}
	fmt.Println(s)

	//ok	sb := strings.Builder{}
	//hg sb := strings.Builder  not an expression
	var sb strings.Builder
	for i := 0; i < 1000; i++ {
		sb.WriteString("x")
	}
	fmt.Println(sb.String())
}
