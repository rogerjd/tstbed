package slice

import (
	"fmt"
	"os"
)

/*
   the slice header is passed around, not the underlying array. the slice hdr
   contains a pointer to the first elem of the array1
   - if slc hdr is modified, can pass ptr to it or return it
*/

func Main(op string) {
	//ref: must use println to close, print wont output till close
	fmt.Println("Slice Main")

	switch op {
	case "make":
		makeSlice()
		return
	case "find":
		findElem()
		return
	case "append":
		appendTst() //write
	default:
		println("op not found: " + op)
	}
}

func findIndex(max int, pred func(i int) bool) int {
	return 3
}

func findElem() {
	//    s := make([]int, 5){1, 2, 3, 4, 5}
	s := []int{1, 2, 3, 4, 5}
	n := findIndex(len(s), func(i int) bool { return false })
	fmt.Printf("index: %d\n", n)
}

func appendTst() {
	fmt.Printf("appendTst")

	var slc []string
	slc = append(slc, "abc")
	////////////////////////

	os.Args = append(os.Args[:1], os.Args[2:]...) // ... are necessary
	/////////////////////////////////////

}
