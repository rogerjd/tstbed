package slice

import (
	"fmt"
	"reflect"
)

func makeSlice() {
	slice1 := make([]int, 10) //ref: capacity defaults to len
	fmt.Println(slice1)

	//from array has num inside[], makes it an array not slice
	array1 := [5]int{} //can init with vals
	fmt.Println(array1)
	fmt.Println(reflect.TypeOf(array1).Kind())

	//A slice literal is declared just like an array literal, except you leave out the element count:
	s := []int{1, 2, 3, 4, 5} //declares array and slice from it
	fmt.Printf("%v %T\n", s, s)

	slice2 := array1[0:5]
	fmt.Println(slice2)
	fmt.Println(reflect.TypeOf(slice2).Kind())
}
