package fmtTest

import (
	"fmt"
	"strconv"
)

func str() {
	fmt.Println("fmt/str")

	const path = "c:/prjs/%s/bkp/"
	s := fmt.Sprintf(path, "prjbkp")
	fmt.Println(s)

	//int to str
	var n int64 = 3
	s = strconv.FormatInt(n, 10)
	fmt.Println(s)
	//str to int
	n, _ = strconv.ParseInt(s, 10, 64)

	fmt.Printf("%6d|%06d\n", 12, 34)
	//leading 0 space, pad left
	//https://play.golang.org/p/Cie2DvblJ1z
}
