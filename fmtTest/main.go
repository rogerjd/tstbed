package fmtTest

import "fmt"

//Main for fmtTest
func Main(cmd string) {

	fmt.Println("fmtTest, cmd:", cmd)

	switch cmd {
	case "str":
		str()
	case "time":
		timeFmt()
	case "scan":
		scan()
	}
}
