package fmtTest

import "fmt"

/*
	errors:
		- input does not match format (misspell: INVOCE)
*/
func scan() {
	ln := "INVOICE=248"
	var inv int
	n, err := fmt.Sscanf(ln, "INVOICE=%d", &inv)
	if err != nil {
		fmt.Print(err)
		return
	}
	fmt.Println(inv, n)
}
