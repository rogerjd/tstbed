package io

import (
	"log"
	"os"
)

func FileName() string {
	var f *os.File
	var err error
	if f, err = os.Open("/home/roger/mygo/src/tstbed/io/tstfile.txt"); err != nil {
		log.Fatalf("could not open file: %s", "tstfile.txt")
	}

	defer f.Close()

	return f.Name()
}
