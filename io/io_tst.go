package io

import (
	//	"strings"
	"bufio"
	"fmt"
	"log"
	"os"
)

func readfile() {
	var f *os.File
	var err error
	if f, err = os.Open("/home/roger/mygo/src/tstbed/io/tstfile.txt"); err != nil {
		log.Fatalf("could not open file: %s", "tstfile.txt")
	}

	defer f.Close()

	//ref: there are alt ways: []byte := ioutile.ReadAll(rdr)
	//todo: see Scanner
	//todo: loop till end
	reader := bufio.NewReader(f)
	for line, err := reader.ReadString('\n'); err == nil; line, err = reader.ReadString('\n') {
		println(line)
	}

	println("reset to beginning of file")
	//ref: doesnt do what i expected reader.Reset(f) switches to another file?? the same one is okay but still at end
	f.Seek(10, 0) //to pos 10 from start, heres how to move to beginning
	for line, err := reader.ReadString('\n'); err == nil; line, err = reader.ReadString('\n') {
		println(line)
	}

	f.Seek(0, 0)
	p := make([]byte, 4)
	n, err := f.Read(p)
	for err == nil {
		println(n, err)
		println(string(p))
		println(p[0], p[1], string(p[2:]))
		n, err = f.Read(p) //p len is not changed, chk n = num of bytes read
		fmt.Printf("%v\n", err)
	}

	//read entire file: see scan

}

//ref: read file line by line
func scanfile() {
	var f *os.File
	var err error
	if f, err = os.Open("C:/Users/roger/go/src/bitbucket.org/rogerjd/tstbed/io/tstfile.txt"); err != nil {
		log.Fatalf("could not open file: %s", "tstfile.txt")
	}

	defer f.Close()

	//ref: there are alt ways: []byte := ioutile.ReadAll(rdr)
	//todo: see Scanner
	println("scan")
	scanner := bufio.NewScanner(f)
	//ref: default SplitFunc breaks on lines \n
	for scanner.Scan() {
		s := scanner.Text()
		println(s) // Println will add back the final '\n'
		//can append to slice string, as well
		/*ref: read delim
		  		rdr := bufio.NewReader(strings.NewReader(s))
		  		for s, err := rdr.ReadString('|'); err == nil; {
		  			println(s)
		  			s, err = rdr.ReadString('|')
		  		}
		          fmt.Println(err)
		*/
	}
	//ref: check errors only once, not after each call to Scan()
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}

func readConsole() {
	rdr := bufio.NewReader(os.Stdin)
	s, _ := rdr.ReadString('\n')
	println(s)

}
