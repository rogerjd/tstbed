package io

import (
	"os"
)

/*
--------------------------------------------------------------------------
   Open -   read only
   Create - write
   Append:
     f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
--------------------------------------------------------------------------
*/

func open_tst() {
	f, err := os.Open("abc")
	if err != nil {
		return
	}

	defer f.Close()
}

/* append
f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
if err != nil {
    panic(err)
}

defer f.Close()

if _, err = f.WriteString(text); err != nil {
    panic(err)
}
*/
