package io

import (
	"io"
	"log"
	"os"
)

//CopyFile copies an existing file to another file(creating a new one if needed)
func copyFile() {
	from, err := os.Open("C:/Users/roger/go/src/bitbucket.org/rogerjd/tstbed/io/srcFile.txt")
	defer from.Close()

	to, err := os.OpenFile("C:/Users/roger/go/src/bitbucket.org/rogerjd/tstbed/io/srcFileCopy.txt", os.O_RDWR|os.O_CREATE, 0666)
	defer to.Close()

	n, err := io.Copy(to, from)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("%d bytes copied", n)
}
