package filepathTst

import (
	"fmt"
	"os"
	"path/filepath"
)

func base() {
	fmt.Print("base")

	const p = "c:/prjs/bkp/file.txt/"

	fn := filepath.Base(p)
	fmt.Print(fn)
}

func walk() {
	filepath.Walk("..", func(path string, info os.FileInfo, err error) error {
		fmt.Println(path, info.Name())
		return nil
	})
}
