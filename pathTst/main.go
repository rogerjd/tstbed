package pathTst

import (
	"fmt"

	"bitbucket.org/rogerjd/tstbed/pathTst/filepathTst"
	"bitbucket.org/rogerjd/tstbed/utils"
)

//Main is menu for sub pkg
func Main(cmd string) {

	fmt.Println("osTest, cmd:", cmd)

	lvl1, lvl2 := utils.GetMenuLevel(cmd)

	switch lvl1 {
	case "filepath":
		filepathTst.Main(lvl2)
	}
}
