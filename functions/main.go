package functions

import "fmt"

//Main is sub menu
func Main(op string) {
	fmt.Println("functions.main")

	//param_assign()

	//ref: must use println to close, print wont output till close
	fmt.Println("Array Main")

	switch op {
	case "make":
	case "fallthrough":
		//doesnt fall through, unless there is fallthrough stmt
		fmt.Printf("fallthrough\n")
	case "param":
		param()
	case "variadic":
		variadicTst()
	case "FuncParam":
		TestFuncParam()
	default:
		println("op not found: " + op)
	}

	//console.log("test snippet")

}
