package functions

import (
	"fmt"
)

func tstReturnParams() (n, m, o int) {
	b := 9
	return 3, b, 6
}

func tst(a, b int, z ...int) {
	println(a, b)
	for _, i := range z {
		println(i)
	}
}

func paramAssign() {
	//ref: must be individually assignable, extra return vals if any go to ...
	tst(tstReturnParams())

	//ref: this is not
	//    println(tstReturnParams())
}

/*
//Main is sub menu
func Main(op string) {
	fmt.Println("functions.main")

	param_assign()

	//ref: must use println to close, print wont output till close
	fmt.Println("Array Main")

	switch op {
	case "make":
	case "fallthrough":
		//doesnt fall through, unless there is fallthrough stmt
		fmt.Printf("fallthrough\n")
	case "param":
		param()
	case "variadic":
		variadicTst()
	default:
		println("op not found: " + op)
	}
}
*/

func variadicTst() {
	//2 ways
	//1) will make new slice to hold these items
	variadic(1, 2, 3)

	//2) will use the existing slice
	n := make([]int, 6)
	variadic(n...)

	variadic()
}

func variadic(n ...int) {
	//if nothing passed, it is len 0
	fmt.Printf("len: %d", len(n))
}
