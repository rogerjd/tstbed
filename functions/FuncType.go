package functions

import "fmt"

type (
	f1 func(name string)
)

func TestFuncParam() {
	test1(a)

	b := func(a string){
			fmt.Printf("b: %s", a)
	}

	test1(b)
}

func a(s string) {
	fmt.Printf("a: %s", s)
}

func test1(f f1) {
	f("test")
}


//test