package functions

import "fmt"

type (
	t  [3]int
	t4 [3]int
	t5 t

	myint int
)

func param() {
	a := [3]int{}
	fmt.Println(a)

	/*
		byValue(a)
		fmt.Println(a)

		byAddr(&a)
		fmt.Println(a)
	*/

	typeParam(a)

	a2 := [2]int{}
	fmt.Println(a2)
	//	typeParam(a2)  error: type mismatch (size)

	t2 := t{}
	typeParam(t2)

	//	t5 := t4{}
	//	typeParam(t5) //err type mismatch: t4 <> t though both same underlying type)

	//type conversions
	var n = 3
	tstType(n)
	var n2 myint = 4
	tstType(int(n2))

	tstType2(myint(n))

}

func byValue(n [3]int) {
	n[0] = 9
	fmt.Printf("in byValue %v\n", n)
}

func byAddr(n *[3]int) {
	n[0] = 9
	fmt.Printf("in byAddr %v\n", n)
}

func typeParam(a t) {
	fmt.Printf("typeParam: %d\n", len(a))
}

//expect: myint and of course int, can be passed
func tstType(n int) {

}

//expect: only myint can be passed
func tstType2(n myint) {

}
