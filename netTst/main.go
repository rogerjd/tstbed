package netTst

import (
	"fmt"

	"bitbucket.org/rogerjd/tstbed/netTst/httpTst"
	"bitbucket.org/rogerjd/tstbed/utils"
)

func Main(cmd string) {

	fmt.Println("DirFilePath, cmd:", cmd)

	lvl1, lvl2 := utils.GetMenuLevel(cmd)
	switch lvl1 {
	case "http":
		httpTst.Main(lvl2)
	}
}
