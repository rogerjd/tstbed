package httpTst

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

//returns Response struct
func get() {
	fmt.Println("get")

	resp, err := http.Get("http://example.com")
	if err != nil {
		fmt.Println("err: " + err.Error())
	}
	fmt.Println(resp.StatusCode)
	res, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(res)
	fmt.Println(string(res))
}
