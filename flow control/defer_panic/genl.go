package defer_panic

func Tst() (res string) {

	//ref: if panic, use recover
	//     if (e := recover(), e != nil) approx
	defer func() {
		res = "abc"
	}()

	res = "def"
	return res
}
