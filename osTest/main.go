package osTest

import "fmt"

//Main is menu for sub pkg
func Main(cmd string) {

	fmt.Println("osTest, cmd:", cmd)

	switch cmd {
	case "mkdir":
		mkdir()
	case "mkalldir":
		mkalldir()
	case "ReadDirNames":
		readdirNames()
	case "curdir":
		curdir()
	}
}
