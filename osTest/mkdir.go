package osTest

import (
	"fmt"
	"os"
)

//Mkdir creates only one dir not multiple, will give error is preceding paths dont exist
//  can use MkdirAll
func mkdir() {
	fmt.Print("mkdir")

	err := os.Mkdir("c:/prjs/bkp", os.ModeDir)
	fmt.Printf("%v", err)
}

func mkalldir() {
	fmt.Print("mkalldir")

	err := os.MkdirAll("c:/prjs/bkp", os.ModeDir)
	fmt.Printf("%v", err)
}
