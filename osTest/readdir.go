package osTest

import (
	"fmt"
	"io/ioutil"
	"os"
)

//also, ioutil.ReadDir(dn) []FileInfo
func readdirNames() {
	dn := "C:/prjs"

	f, err := os.Open(dn)
	if err != nil {
		fmt.Printf("%v", err)
	}

	names, err := f.Readdirnames(-1) //[]string
	fmt.Print(names)
	f.Close()

	f, _ = os.Open(dn)
	infos, err := f.Readdir(-1) //[]FileInfo
	fmt.Print(infos)
	f.Close()

	fi, err := ioutil.ReadDir(dn) //[]FileInfo  input in string, directory name
	fmt.Print(fi)
}
