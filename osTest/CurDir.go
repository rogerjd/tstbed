package osTest

import (
	"fmt"
	"os"
)

func curdir() {
	/*
		where exe started : os.Args[0]
		-  os.Getwd()
		-  ex, err := os.Executable()

			if err != nil {

				panic(err)

			}

			exPath := filepath.Dir(ex)

			fmt.Println(exPath)
	*/

	fmt.Println(os.Getwd())      //dir
	fmt.Println(os.Executable()) //exe & dir
	fmt.Println(os.Args[0])

}
