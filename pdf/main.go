package main

import (
	"fmt"
	"github.com/jung-kurt/gofpdf"
)

func main() {
	println("a")
	pdf := gofpdf.New("P", "mm", "A4", "")
	pdf.AddPage()
	pdf.SetFont("Arial", "B", 16)
	pdf.Cell(40, 10, "Hello, world")
	pdf.SetFont("Courier", "", 10)
	pdf.Cell(80, 200, "Gross Pay")
	pdf.Cell(80, 200, "Pay")
	pdf.AddPage()
	pdf.SetXY(0, 0)
	pdf.Cell(80, 10, "Net Pay")
	pdf.Rect(25, 15, 75, 150, "")
	pdf.Image("/home/roger/mygo/src/pdf/ibm.jpg", 2, 3, 55, 55, true, "", 0, "")
	//ok if running from home    pdf.Image("mygo/src/pdf/ibm.jpg", 2, 3, 55, 55, true, "", 0, "")
	err := pdf.OutputFileAndClose("hello.pdf")
	fmt.Printf("%v", err)
}
