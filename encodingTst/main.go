package encodingTst

import (
	"fmt"

	"bitbucket.org/rogerjd/tstbed/encodingTst/binaryTst"
	"bitbucket.org/rogerjd/tstbed/utils"
)

func Main(cmd string) {

	fmt.Println("encoding, cmd:", cmd)

	lvl1, lvl2 := utils.GetMenuLevel(cmd)
	switch lvl1 {
	case "binary":
		binaryTst.Main(lvl2)
	}
}
