package binaryTst

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

func Main(cmd string) {

	fmt.Println("encoding/binary, cmd:", cmd)

	switch cmd {
	case "read":
		read()
	}
}

func read() {
	var n int
	b := []byte{0x12, 0x34, 0x56, 0x78}
	rdr := bytes.NewReader(b)
	binary.Read(rdr, binary.BigEndian, &n)
	fmt.Println(n)
}
