package channel

import (
	"fmt"
)

func gen(nums ...int) <-chan int {
	out := make(chan int)

	go func() {
		fmt.Println("gen: before range")
		for _, n := range nums {
			fmt.Printf("gen: before send: %d\n", n)
			out <- n
			fmt.Println("gen: after send")
		}
		close(out)
		fmt.Println("gen: chan closed")
	}()
	return out
}

func sqr(in <-chan int) <-chan int {
	out := make(chan int)
	go func() {
		fmt.Println("sqr: before range")
		for n := range in {
			fmt.Printf("sqr: before send: %d\n", n)
			out <- n * n
			fmt.Println("sqr: after send")
		}
		close(out)
		fmt.Println("sqr: chan closed")
	}()
	return out

}

func excerise1() {
	c := gen(2, 3)
	c1 := sqr(c)

	fmt.Println("print resuits")
	fmt.Println(<-c1)
	fmt.Println(<-c1)
}
