package utils

import "strings"

//ref: here the idea is we can subdivide str tst; ie: str/replace, etc
func GetMenuLevel(arg string) (lvl0, lvl1 string) {
	p := strings.Index(arg, "/")
	if p == -1 {
		lvl0 = arg
		return lvl0, lvl1
	}
	lvl0 = arg[:p]
	lvl1 = arg[p+1:]
	return lvl0, lvl1
}
