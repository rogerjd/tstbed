package bufioTst

import (
	"bufio"
	"fmt"
	"os"
)

func scan() {
	//ok	f, err := os.Open("c:/Users/roger/go/src/bitbucket.org/rogerjd/tstbed/bufioTst/data.txt")
	f, err := os.Open("../bufioTst/data.txt")
	if err != nil {
		fmt.Print(err)
		return
	}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		fmt.Println(scanner.Text())

	}

}
