package regexp2

import (
	"fmt"
	"regexp"
)

/*
Submatches are matches of
// parenthesized subexpressions (also known as capturing groups) within the
// regular expression, numbered from left to right in order of opening
// parenthesis. Submatch 0 is the match of the entire expression, submatch 1
// the match of the first parenthesized subexpression, and so on.
*/
func tstFindAllSubmatch() {
	re := regexp.MustCompile("a(x*)b")
	b := []byte("axbaxxbc")
	fmt.Printf("%v\n", re.FindAllSubmatch(b, -1))

	fmt.Println("no constant part")
	re = regexp.MustCompile(".*")
	b = []byte("axbaxxbc")
	fmt.Printf("%v", re.FindAllSubmatch(b, -1)) //2nd param: num of matches, < 0 = all
}

/*
func TstFindAllSubmatch2(r io.Reader){
    re := regexp.MustCompile("a(x*)b")
    b := []byte("axbaxxbc")
    fmt.Printf("%v", re.FindAllSubmatch(b, -1))
}
*/
