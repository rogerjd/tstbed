package http

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

//A Client is an HTTP client. Its zero value (DefaultClient); http.Get() is a wrapper around DC.Get()

//use net/http

//request get
func getTst() {
	resp, err := http.Get("http://www.wsj.com")
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()

	fmt.Print(resp.Status)

	//fmt.Printf("%v %v\n", resp, err)

	bdy, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Print(err)
		return
	}

	fmt.Print(bdy)
	fmt.Print(string(bdy))
	resp, err = http.Get("http://www.qtrac.eu/index.html")
	if err != nil {
		fmt.Print(err)
		return
	}
	defer resp.Body.Close()
	fmt.Printf("%v %v\n", resp, err)
}
