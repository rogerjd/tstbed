package directory

/*
func contents() {
	fis, err := ioutil.ReadDir(prjsPath) //[]os.FileInfo  input is string, directory name
	if err != nil {
		log.Fatal(err)
	}
	count := 0
	prjName := ""
	for _, fi := range fis {
		if fi.IsDir() {
			count++
			prjName = fi.Name()
		}
	}
	if count != 1 {
		return "", errors.New("Prd Dir not found or found multiple: " + prjNum)
	}
	return prjName, nil
}
*/

//func create() {
//Mkdir creates only one dir not multiple, will give error is preceding paths dont exist
//  can use MkdirAll
/*
   func mkdir() {
   	fmt.Print("mkdir")

   	err := os.Mkdir("c:/prjs/bkp", os.ModeDir)
   	fmt.Printf("%v", err)
   }

   func mkalldir() {
   	fmt.Print("mkalldir")

   	err := os.MkdirAll("c:/prjs/bkp", os.ModeDir)
   	fmt.Printf("%v", err)
   }
*/
//}
