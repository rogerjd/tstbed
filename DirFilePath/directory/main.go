package directory

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

//Main for directory
func Main(cmd string) {

	fmt.Println("directory, cmd:", cmd)

	switch cmd {
	case "contents":
		contents()
	case "create":
		create()
	case "remove":
		remove()
	case "tempdir":
		tempDir()
	}
}

func contents() {
	const prjsPath = "c:/temp/"

	fis, err := ioutil.ReadDir(prjsPath) //[]os.FileInfo  input is string, directory name
	if err != nil {
		log.Fatal(err)
	}
	count := 0
	for _, fi := range fis {
		if fi.IsDir() {
			count++
			fmt.Print(fi.Name())
		}
	}
	fmt.Printf("total: dirs %d, dir entries: %d", count, len(fis))
}

func create() {
	err := os.Mkdir("c:/temp/dir1", os.ModeDir) //only creates 1 dir, will give err 3 if creating multi
	if err != nil {                             //error, if already exists
		log.Fatal(err)
	}

	err = os.MkdirAll("c:/temp/dira/dirb", os.ModeDir) //will create all, multi
	if err != nil {                                    //error, if already exists
		log.Fatal(err)
	}
}

//delete dir/file
func remove() {
	err := os.Remove("c:/temp")
	if err != nil { //error 145: the directory is not empty
		log.Print(err)
	}

	err = os.RemoveAll("c:/temp") //removes, even if not empty, and sub direcotries
	if err != nil {
		log.Print(err)
	}
}
