package dirfilepath

import (
	"fmt"

	"bitbucket.org/rogerjd/tstbed/DirFilePath/directory"
	"bitbucket.org/rogerjd/tstbed/DirFilePath/file"
	"bitbucket.org/rogerjd/tstbed/utils"
)

//Main for DirFilePath
func Main(cmd string) {

	fmt.Println("DirFilePath, cmd:", cmd)

	lvl1, lvl2 := utils.GetMenuLevel(cmd)

	switch lvl1 {
	case "directory":
		directory.Main(lvl2)
	case "file":
		file.Main(lvl2)
	case "time":
		//timeFmt()
	}
}
