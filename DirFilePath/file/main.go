package file

import (
	"fmt"
	"io"
	"log"
	"os"
)

//Main for file
func Main(cmd string) {

	fmt.Println("file, cmd:", cmd)

	switch cmd {
	case "copy":
		copy()
	case "read":
		read()
	}
}

//CopyFile copies an existing file to another file(creating a new one if needed)
func copy() {
	from, err := os.Open("C:/Users/roger/go/src/bitbucket.org/rogerjd/tstbed/io/srcFile.txt")
	defer from.Close()

	to, err := os.OpenFile("C:/Users/roger/go/src/bitbucket.org/rogerjd/tstbed/io/srcFileCopy.txt", os.O_RDWR|os.O_CREATE, 0666)
	defer to.Close()

	n, err := io.Copy(to, from)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("%d bytes copied", n)
}

//reset rewind seek
/*
Seeking to the beginning of the file is easiest done using File.Seek(0, 0) just as you suggested, but don't forget that:
The behavior of Seek on a file opened with O_APPEND is not specified.
*/
func reset() {

}

func read() {

}
