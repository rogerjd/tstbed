package interfacetst

type (
	frm interface {
		Over() bool
		Score() int
		RecordRoll(r int)
	}

	regframe struct {
		roll1, roll2 int
		rollptr      int
	}

	finalframe struct {
		*regframe
		roll3 int
	}
)

func (rf regframe) Over() bool {
	println("reg frame Over")
	return rf.rollptr == 2
}

func (rf *regframe) RecordRoll(r int) {
	println("reg record roll")
	rf.rollptr++
	switch rf.rollptr {
	case 1:
		rf.roll1 = r
	case 2:
		rf.roll2 = r

	}
}

func (rf *regframe) Score() int {
	println("reg frame Over")
	return 0
}

func (ff finalframe) Over() bool {
	println("final frame Over")
	return ff.regframe.rollptr == 3
}

func (ff *finalframe) RecordRoll(r int) {
	println("final record roll")
	ff.roll3 = r
}

func (ff finalframe) Score() int {
	println("final frame Over")
	return 0
}

func test() {
	var a [3]frm
	rf := regframe{}
	var f frm = &regframe{}
	ff := finalframe{regframe: &rf}
	a[0] = &rf
	a[1] = f
	a[2] = &ff

	a[0].RecordRoll(7)
	a[0].Over()
	a[0].RecordRoll(2)
	a[0].Over()

	a[1].Over()
}
