package main

//test

import (
	"fmt"
	"os"

	"bitbucket.org/rogerjd/tstbed/interfacetst"

	dirfilepath "bitbucket.org/rogerjd/tstbed/DirFilePath"
	"bitbucket.org/rogerjd/tstbed/bufioTst"
	"bitbucket.org/rogerjd/tstbed/builtintst"
	"bitbucket.org/rogerjd/tstbed/channel"
	"bitbucket.org/rogerjd/tstbed/encodingTst"
	"bitbucket.org/rogerjd/tstbed/io"
	"bitbucket.org/rogerjd/tstbed/osTest"
	"bitbucket.org/rogerjd/tstbed/pathTst"
	"bitbucket.org/rogerjd/tstbed/sort"
	"bitbucket.org/rogerjd/tstbed/timeTst"

	"runtime"

	"bitbucket.org/rogerjd/tstbed/datatypes/array"
	"bitbucket.org/rogerjd/tstbed/datatypes/jsonTst"
	"bitbucket.org/rogerjd/tstbed/datatypes/slice"
	"bitbucket.org/rogerjd/tstbed/datatypes/str"
	"bitbucket.org/rogerjd/tstbed/datatypes/structtst"
	"bitbucket.org/rogerjd/tstbed/fmtTest"
	"bitbucket.org/rogerjd/tstbed/functions"
	"bitbucket.org/rogerjd/tstbed/http"
	"bitbucket.org/rogerjd/tstbed/netTst"
	"bitbucket.org/rogerjd/tstbed/regexp2"
	"bitbucket.org/rogerjd/tstbed/utils"
	//ref: import cycle error	"tstbed/pkg_a"
	//	"tstbed/pkg_b"
	//	"tstbed/type_assertion"
	//	"tstbed/defer_panic"
	//	"tstbed/pkgs, builtin/os"
)

//ref: no args, no return value
func main() {
	println(runtime.Version())
	println("the name of the cmd/pgm is: " + os.Args[0])
	println(len(os.Args))
	if len(os.Args) == 1 {
		fmt.Printf("usage: enter pkg to test/subpkg\n")
		os.Exit(1)
		//todo: list of pkgs??
	}

	pkg := os.Args[1]
	//	println(pkg)
	//	p := strings.Index(pkg, "/")
	pkg1, pkg2 := utils.GetMenuLevel(pkg)
	//pkg1 := pkg[:p]
	//	pkg2 := pkg[p+1:]
	println(pkg1, pkg2)
	switch pkg1 {
	case "io":
		io.Main(pkg2)
		return
	case "bufio":
		bufioTst.Main(pkg2)
	case "str":
		str.Main(pkg2)
		return
	case "os":
		osTest.Main(pkg2)
	case "fmt":
		fmtTest.Main(pkg2)
	case "path":
		pathTst.Main(pkg2)
	case "time":
		timeTst.Main(pkg2)
	case "func":
		functions.Main(pkg2)
		return
	case "slice":
		slice.Main(pkg2)
		return
	case "typeassertion":
		//	    type_assertion.Tst()
		return
	case "defer":
		//	    fmt.Println(defer_panic.Tst())
		return
	case "sort":
		sort.Main(pkg2)
		return
	case "http":
		http.Main(pkg2)
		return
	case "regexp":
		regexp2.Main(pkg2)
		return
	case "pkg":
		//	    pkg.Main(pkg2)
		return
	case "array":
		array.Main(pkg2)
		return
	case "dirfilepath":
		dirfilepath.Main(pkg2)
	case "json":
		jsonTst.Main(pkg2)
	case "net":
		netTst.Main(pkg2)
	case "encoding":
		encodingTst.Main(pkg2)
	case "channel":
		channel.Main(pkg2)
	case "builtin":
		builtintst.Main(pkg2)
	case "interface":
		interfacetst.Main(pkg2)
	case "struct":
		structtst.Main(pkg2)
	default:
		println("could not find: " + pkg)
	}
}
