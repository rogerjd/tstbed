package timeTst

import (
	"fmt"
	"time"
)

//Main is menu for sub pkg
func Main(cmd string) {

	fmt.Println("timeTst, cmd:", cmd)

	switch cmd {
	case "now":
		fmt.Print(time.Now())
	}
}
