package sort

import (
	"fmt"
	"sort"
)

const (
	fed = iota
	state
	local
)

type (
	TaxCode struct {
		//        LnNo int
		Lvl,
		Auth,
		TaxCode string
	}

	TaxCodeSorter []TaxCode

	TaxDtl struct {
		LnNo int
		Code string
		Tax  int
	}

	TaxDtls []TaxDtl
)

func (tds TaxDtls) Len() int {
	return len(tds)
}

func (tds TaxDtls) Swap(i, j int) {
	tds[i], tds[j] = tds[j], tds[i]
}

func (tds TaxDtls) Less(i, j int) bool {
	return tds[i].LnNo < tds[j].LnNo
}

func (tx TaxCodeSorter) Len() int {
	return len(tx)
}

func (tx TaxCodeSorter) Swap(i, j int) {
	tx[i], tx[j] = tx[j], tx[i]
}

func (tx TaxCodeSorter) Less(i, j int) bool {
	if tx[i].Lvl == "f" {
		if tx[j].Lvl != "f" {
			return true
		}
		//do taxes
	} else if tx[i].Lvl == "s" {
		if tx[j].Lvl == "f" {
			return false
		} else if tx[j].Lvl == "l" {
			return true
		} else if tx[j].Lvl == "s" {
			if tx[i].Auth != tx[j].Auth {
				return tx[i].Auth < tx[j].Auth
			}
			//do taxes all in one func?
			//if state specific, make specific sort func
		}
	} else if tx[i].Lvl == "l" {
		if tx[j].Lvl != "l" {
			return false
		}
		if tx[i].Auth != tx[j].Auth {
			return tx[i].Auth < tx[j].Auth
		}
		//do taxes all in one func?

		//do taxes all in one func?
	}
	return true
}

func (tx TaxCodeSorter) Sort(i, j int) bool {
	if tx[i].TaxCode == "it" {
		return true
	} else if tx[i].TaxCode == "sst" {
		if tx[j].TaxCode == "it" {
			return false
		}
		return true
	} else if tx[i].TaxCode == "mt" {
		if tx[j].TaxCode == "it" || tx[j].TaxCode == "sst" {
			return false
		}
		return true
	} else if tx[i].TaxCode == "sui" {
		if tx[j].TaxCode == "it" || tx[j].TaxCode == "sst" || tx[j].TaxCode == "mt" {
			return false
		}
		return true
	} else if tx[i].TaxCode == "sdi" {
		if tx[j].TaxCode == "it" || tx[j].TaxCode == "sst" || tx[j].TaxCode == "mt" || tx[j].TaxCode == "sui" {
			return false
		}
		return true //only need  return = expr   //todo:
	} else if tx[i].TaxCode == "fli" {
		return false
	}
	return false
}

func tst() {
	tds := TaxDtls{{3, "mt", 43}, {1, "it", 98}, {2, "sst", 37}}
	fmt.Println(tds)
	sort.Sort(tds)
	fmt.Println(tds)

	ts := TaxCodeSorter{{"F", "F", "IT"}}
	sort.Sort(ts)
	fmt.Println(ts)
}
