package sort

import (
	"fmt"
	"sort"
)

func Main(cmd string) {
	fmt.Println("sort main, cmd:", cmd)
	switch cmd {
	case "tst":
		fmt.Println("  tst")
		tst()
		return
	case "strings":
		fmt.Println("  strings")
		s := []string{"z", "a"}
		fmt.Println(s)
		sort.Strings(s)
		fmt.Println(s)
		return
	case "search":
		Searchtst()
	default:
		fmt.Println("  cmd not found")
	}
}
