package sort

import (
	"fmt"
	"sort"
)

//Search uses binary search to find and return the smallest index i in [0, n)
//at which f(i) is true, assuming that on the range [0, n), f(i) == true
//implies f(i+1) == true. That is, Search requires that f is false for some (possibly

//there are 2 func here
func Searchtst() {
	fmt.Println("sort search")

	x := 23
	a := []int{6, 23, 87, 4, 43, 9}
	sort.Ints(a)

	//SearchInts searches for x in a sorted slice of strings and returns the index
	//as specified by Search. The return value is the index to insert x if x is not present (it could be len(a)). The slice must be sorted in ascending order.
	i := sort.SearchInts(a, 23)
	fmt.Println(i)

	//Search requires that f is false for some (possibly empty) prefix of the input range [0, n) and then true for the (possibly empty) remainder
	i = sort.Search(len(a), func(i int) bool {
		fmt.Println(i)
		return a[i] >= x
	}) //must use >=  not ==   see above
	if i < len(a) && a[i] == x { //here the equal test is preformed
		fmt.Printf("found %d at index %d  in array/slice:  %v\n", x, i, a)
	} else {
		fmt.Println("not found")
	}

}
