package newarray

import (
	"fmt"
)

func Range_Tst() {
	a := [...]int{1, 2, 6, 7}
	for i, v := range a {
		fmt.Printf("%v %v", i, v)
	}
}
