package statements

import (
	"fmt"
)

func Switch_Tst(n int) {
	switch {
	case n <= 5:
		fmt.Println("its less than or equal to 5")
	default:
		fmt.Println("its greater than 5")
	}
}
