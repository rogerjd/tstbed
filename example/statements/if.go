package statements

func tst() {
	var x int64 = 0

	if x == 0 {
		print(x)
	}
}
