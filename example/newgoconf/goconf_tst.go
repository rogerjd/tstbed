package newgoconf

import (
	"fmt"
	"github.com/dlintw/goconf"
	"strings"
)

func Tst_goconf() {
	c, err := goconf.ReadConfigFile("newgoconf/tbl_tst.txt")
	if err != nil {
		fmt.Println("error: " + err.Error())
		return
	}
	fmt.Println(c)

	s, err := c.GetString("tax", "table")
	if err != nil {
		fmt.Println("error: " + err.Error())
		return
	}
	fmt.Println(s)

	t := strings.Split(s, "\n")
	fmt.Println(t)
	for _, ln := range t {
		fmt.Println(ln)
	}

	fmt.Println(strings.Count(s, "\n"))
	fmt.Println(strings.Index(s, "\n"))
	for s != "" {
		p := strings.Index(s, "\n")
		if p > -1 {
			fmt.Println(s[0:p])
			s = s[p+1:]
		} else {
			fmt.Println(s)
			s = ""
		}

	}

}
