package newfile

import (
	"os"
)

func OpenFile_Tst(fn string) {
	f, err := os.Open(fn)
	//ref: err is not a str  todo: File.Error() ???
	if err != nil {
		print("an err occurred")
	} else {
		f.Close()
	}
}
