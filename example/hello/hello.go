package main

import (
	"example/db/sqlite_tst"
	"example/newarray"
	"example/newdatatypes"
	"example/newdatatypes/godec"
	"example/newdatatypes/strings"
	"example/newfile"
	"example/newgoconf"
	"example/newioutil"
	"example/newjson"
	"example/newmath"
	"example/newstr"
	"example/statements"
	"fmt"
)

func main() {
	fmt.Printf("sql %s", "tst")
	fmt.Printf("Hello, world!!!!. Sqrt(2) = %v\n", newmath.Sqrt(2))
	newstr.Range_tst()
	newstr.Tst()
	print(newstr.StrLen_Tst("abc"))
	newarray.Range_Tst()
	print(newstr.OrdOfNumStr_Tst('3'))
	statements.Switch_Tst(4)
	newfile.OpenFile_Tst("abc")
	newioutil.WriteFile()
	newdatatypes.Pointer_Tst()
	newdatatypes.ReceiverIsPointer()
	fmt.Println("sql")
	sqlite_tst.Sqlite_ex()
	newdatatypes.Slice_tst()
	newgoconf.Tst_goconf()
	strings.String_tst()
	strings.Split_tst()
	strings.Atoi_tst("3")
	strings.Atoi_tst("3a")
	godec.Tst()
	newjson.Marshal_test()
	strings.SliceToStr()
	strings.StrToSlice()

}
