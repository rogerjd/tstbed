package newjson

import "encoding/json"
import "fmt"

type Message struct {
	Name string
	Body string
	Time int64
}

func Marshal_test() {
	m := Message{"Alice", "Hello", 1249706}
	b, err := json.Marshal(m)
	if err != nil {
		print(err)
		return
	} else {
		println(string(b))
	}

	//decode
	var m1 Message
	err = json.Unmarshal(b, &m1)
	if err != nil {
		print(err)
	} else {
		fmt.Printf("%+v", m1)
	}

}
