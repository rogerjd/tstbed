package newdatatypes

import (
	"fmt"
)

//todo:
/* p = &n
 *p = n
 */

//ref:  Selectors automatically dereference pointers to structs
//      not so for reg ptrs
func Pointer_Tst() {
	var (
		p *int
		n int
	)

	fmt.Print(p, &p)
	fmt.Print(n)

	n = 3

	/*ref: err
	  *p = n
	  fmt.Print(p)
	*/

	p = &n
	fmt.Print(p, *p)

	*p = 5
	fmt.Println("compare: *p n", *p, n)
}

type ByteSlice []byte

func (p *ByteSlice) Write(data []byte) (n int, err error) {
	slice := *p

	slice[0] = 65
	slice[1] = 66

	*p = slice
	return 0, nil
}
