package strings

import "fmt"

func SliceToStr() {
	b := []byte{65, 66, 67}
	println(string(b))
}

func StrToSlice() {
	s := "{name: value}"
	fmt.Printf("%+v", []byte(s))
}
