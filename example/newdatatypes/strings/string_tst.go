package strings

import (
	"fmt"
	"strconv"
	"strings"
)

func String_tst() {
	fmt.Println("string")
}

func Split_tst() {
	fmt.Println("split")

	s := "a, b, c"

	sl := strings.Split(s, ",")
	fmt.Println(sl)
	fmt.Println(len(sl))
	for i, v := range s {
		fmt.Println(i, v)
	}
	for i, v := range sl {
		fmt.Println(i, v)
	}

	//    s = "12, 34, 56" + "\n" + "77, 88, 99"
	s = "12.12 34.54 56.55\n77.88 88 99.00"
	fmt.Println(s)
	sl = strings.Split(s, "\n")
	fmt.Println(sl)
	fmt.Println(len(sl))
	for i, v := range sl {
		fmt.Println(i, v)

		f := strings.Fields(v)
		fmt.Println(f)
		for i := 0; i < len(f); i++ {
			fmt.Println(f[i])
		}

		//       for _, f := range(strings.Fields(v)){
		//            fmt.Println(f)
		//        }
	}
}

func Atoi_tst(s string) {
	i, err := strconv.Atoi(s)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println(i)
}
