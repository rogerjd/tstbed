package godec

import (
	"code.google.com/p/godec/dec"
	"fmt"
)

func Tst() {
	fmt.Println("go dec test")

	d := new(dec.Dec)
	d.SetString("012345.67890") // decimal; leading 0 ignored; trailing 0 kept
	fmt.Println(d)

	a := new(dec.Dec)
	a.SetString("3.01")
	b := new(dec.Dec)
	b.SetString("4.2")
	d = d.Add(a, b)
	fmt.Println(d)
	if i := a.Cmp(b); i == -1 {
		fmt.Println("a < b as expected")
	} else {
		fmt.Println("a not < b ERROR")
	}

	//	f := dec.NewDec(100, 2);
	//	fmt.Println(f);
}
