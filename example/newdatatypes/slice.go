package newdatatypes

import (
	"fmt"
)

func Slice_tst() {
	fmt.Println("tst")

	s := []byte{'g', 'o', 'l', 'a', 'n', 'g'}
	s2 := s[2:4]
	fmt.Println(string(s), string(s2))
	s2[0] = 'x'
	fmt.Println(string(s), string(s2))
	s2[0] = 'l'
	fmt.Println(string(s), string(s2))
	fmt.Println(len(s), cap(s))
}
