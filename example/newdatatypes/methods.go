package newdatatypes

import (
	"fmt"
)

type Page struct {
	Title string
	Body  string
}

func (p *Page) save() error {
	//ref: i dont know why dont need to deref
	// Selectors automatically dereference pointers to structs
	fmt.Println("page.title is: ", p.Title)
	p.tst_no_rcvr_id()
	return nil
}

func (*Page) tst_no_rcvr_id() {
	fmt.Println("no identifier for receiver")
}

func ReceiverIsPointer() {
	pg := &Page{Title: "my page"}
	pg.save()
}
