package newstr

import (
	"fmt"
)

func Range_tst() {
	for k, v := range "abc" {
		fmt.Printf("%d %v", k, v)
	}
}

func StrLen_Tst(s string) int {
	return len(s)
}

func OrdOfNumStr_Tst(c byte) byte {
	return c - '0'
}
