package builtintst

import (
	"fmt"
)

func Main(cmd string) {
	fmt.Println("builtintst, cmd:", cmd)
	switch cmd {
	case "tst":
		fmt.Println("  tst")
	case "copy":
		copytst()
	default:
		fmt.Println("  cmd not found")

	}
}
