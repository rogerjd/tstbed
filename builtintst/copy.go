package builtintst

import (
	"fmt"
	"strings"
)

//copies min of dest, source

func copytst() {
	b := make([]byte, 3)

	fmt.Println(b)
	copy(b[0:], "a")
	fmt.Println(b)
	copy(b[1:], "b")
	fmt.Println(b)
	copy(b[2:], "c")
	fmt.Println(b)

	fmt.Println(b)
	copy(b[0:], "a")
	fmt.Println(b)
	copy(b[1:], "b")
	fmt.Println(b)
	copy(b[2:], "c")
	fmt.Println(b)

	copy(b[0:], "000")                  //strings.Repeat ?
	copy(b[0:], strings.Repeat("e", 9)) //size, length, copeie
	fmt.Println(b)
	copy(b[1:2], "z")
	fmt.Println(b)
}
