//ref: these read strings (or bytes) & are buffered
       the io pkg is unbuffered and works with raw bytes 
 
(inFile io.Reader, outFile io.Writer) 
//these can be Files as they are readers and writers (interface)

reader := bufio.NewReader(inFile)
writer := bufio.NewWriter(outFile)



var line string
line, err = reader.ReadString('\n')
if err == io.EOF {
err = nil
// io.EOF isn't really an error
eof = true // this will end the loop at the next iteration
} else if err != nil {
return err // finish immediately for real errors
}

